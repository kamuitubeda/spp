<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Rekening;
use App\Models\Item;

class RincianRekening extends Model
{
    use HasFactory;

    protected $fillable = [
        'rekening_id',
        'item_id'
    ];
}
