<?php

namespace App\Filament\Resources\InstitusiResource\Pages;

use App\Filament\Resources\InstitusiResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ViewRecord;

class ViewInstitusi extends ViewRecord
{
    protected static string $resource = InstitusiResource::class;

    protected function getActions(): array
    {
        return [
            Actions\EditAction::make(),
        ];
    }
}
