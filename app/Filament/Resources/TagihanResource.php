<?php

namespace App\Filament\Resources;

use App\Filament\Resources\TagihanResource\Pages;
use App\Filament\Resources\TagihanResource\RelationManagers;
use App\Models\Tagihan;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class TagihanResource extends Resource
{
    protected static ?string $model = Tagihan::class;
    protected static ?string $navigationGroup = 'Manajemen Pembayaran';
    protected static ?int $navigationSort = 7;
    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('rekening_id')
                    ->required(),
                Forms\Components\TextInput::make('santri_id')
                    ->required(),
                Forms\Components\TextInput::make('pencatat_pelunasan_id'),
                Forms\Components\TextInput::make('pencatat_pengeluaran_id'),
                Forms\Components\Toggle::make('lunas')
                    ->required(),
                Forms\Components\DateTimePicker::make('tanggal_pelunasan'),
                Forms\Components\DateTimePicker::make('tanggal_pengeluaran'),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('rekening_id'),
                Tables\Columns\TextColumn::make('santri_id'),
                Tables\Columns\TextColumn::make('pencatat_pelunasan_id'),
                Tables\Columns\TextColumn::make('pencatat_pengeluaran_id'),
                Tables\Columns\BooleanColumn::make('lunas'),
                Tables\Columns\TextColumn::make('tanggal_pelunasan')
                    ->dateTime(),
                Tables\Columns\TextColumn::make('tanggal_pengeluaran')
                    ->dateTime(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\ViewAction::make(),
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTagihans::route('/'),
            'create' => Pages\CreateTagihan::route('/create'),
            'view' => Pages\ViewTagihan::route('/{record}'),
            'edit' => Pages\EditTagihan::route('/{record}/edit'),
        ];
    }    
}
